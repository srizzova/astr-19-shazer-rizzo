# This program will print out your full name, your preferred pronouns (optional), and two sentences about your favorite movie and your favorite food.

# function to print string of name and pronouns
def PrintNamePronouns():
	print("My Name is Shazer Rizzo Varela (She/They)")

# Prints a string of a few sentencens
def PrintFav():
	print("My favorite movie is Airplane and my favorite food is pizza")

# main runs the defined functions
def main():
    PrintNamePronouns() 
    PrintFav()

# when this program runs, main runs
if __name__=="__main__":
	main()