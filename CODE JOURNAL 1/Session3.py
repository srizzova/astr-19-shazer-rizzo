#Shazer Rizzo
#Write a Python program that defines a function f(x) that returns x**3 + 8. In the main function of the program, call f(x) with x = 9 and print the result.  Use an if statement that executes if the result is larger than 27 and prints “YAY!”.

# The F(x) function
def f(x):
    RETURNABLE = (x**3) + 8
    return RETURNABLE


# main runs the defined functions
def main():
    STATMENTP = f(9)
    print(STATMENTP)
    if (STATMENTP > 27):
        print("YAY!")

# when this program runs, main runs
if __name__=="__main__":
	main()
