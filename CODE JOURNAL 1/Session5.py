# Shazer Rizzo
# Write a Python program that writes out a table of the function sin(x) vs. x, where x is tabulated between 0 and 2 with a thousand entries. Follow the basic Python program structure, including a main program function.
# imports
import numpy as np
import math

#MathArray = np.array([1.0,1.2,3.5])
#np.linspace
#np.arange
#x = np.linspace(0,100,5)
Var2 = math.pi * 2
x = np.linspace(0,Var2,1000) # X
#y = np.linspace(1,1000, 1000) # Sin

def Session():
    Var1 = 0
    for i in range(len(x)):
        Var1 = math.sin(x[i])
        print(x[i], Var1)
        i += 1

# main runs the defined functions
def main():
    print("X","Sin(X)")
    print("---------")
    #for i in range(len(x)):
    #    print(x[i],y[i])
    Session()

# when this program runs, main runs
if __name__=="__main__":
	main()