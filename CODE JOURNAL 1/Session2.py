# Shazer Rizzo
 #Write a Python program that prints the sum of two floating point numbers, the difference between two integers, and the product of a floating point number and an integer. In each case, have the program print out the data type of the resulting answer.

# function the sum of two floating point numbers
def SumsFloat():
    Number1 = float(10)
    Number2 = float(2)
    Sumsoftwo = Number1 + Number2
    print(Sumsoftwo)
    print(type(Sumsoftwo))

# Prints the difference between two integers
def DiffIntegers():
    Number1 = int(12)
    Number2 = int(7)
    DifferenceTwo = Number1 - Number2
    print(DifferenceTwo)
    print(type(DifferenceTwo))

# Prints the product of a floating point number and an integer
def ProdFloatInt():
    Number1 = float(12.1)
    Number2 = int(7)
    ProdTwo = Number1 * Number2
    print(ProdTwo)
    print(type(ProdTwo))

# main runs the defined functions
def main():
    SumsFloat()
    DiffIntegers()
    ProdFloatInt()

# when this program runs, main runs
if __name__=="__main__":
	main()