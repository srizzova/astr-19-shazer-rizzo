#Shazer Rizzo
#Write a Python program that declares a class describing your favorite animal. Have the data members of the class represent the following physical parameters of the animal: length of the arms (float), length of the legs (float), number of eyes (int), does it have a tail? (bool), is it furry? (bool). Write an initialization function that sets the values of the data members when an instance of the class is created. Write a member function of the class to print out and describe the data members representing the physical characteristics of the animal.

class Dog:
    def __init__(self,LengthArm, LengthLeg, NumberEyes, Tail, Furry):
        self.LengthArm = LengthArm
        self.LengthLeg = LengthLeg
        self.NumberEyes = NumberEyes
        self.Tail = Tail
        self.Furry = Furry

    def fun(self):
        print("Here is your Dog:")
        print("Length of arms:",self.LengthArm,"CM")
        print("Length of Legs:",self.LengthLeg, "CM")
        print("Number of eyes:",self.NumberEyes)
        print("They have a Tail:",self.Tail)
        print("They are Furry:",self.Furry)


# main runs the defined functions
def main():
    Boxer = Dog(54.2,55.3,2,True,True)
    Boxer.fun()

# when this program runs, main runs
if __name__=="__main__":
	main()

