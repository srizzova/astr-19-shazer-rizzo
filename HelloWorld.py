# This program will write Hello World!

# function to print string
def PrintHelloWorld():
	print("Hello World!")

# main runs the defined functions
def main():
	PrintHelloWorld()

# when this program runs, main runs
if __name__=="__main__":
	main()
